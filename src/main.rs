use std::io::prelude::*;
use std::io;
use std::fs::File;
use std::collections::{HashMap, HashSet};

#[derive(Hash, PartialEq, Eq, Clone)]
struct IndexEntry {
    filename: String,
    word: String,
    line: i64,
}

struct Index {
    index_store: HashMap<String, HashSet<IndexEntry>>,
    files: HashSet<String>
}

impl IndexEntry {
    fn new(filename: &str, word: &str, line: i64) -> IndexEntry {
        IndexEntry{
            filename: filename.to_string(),
            word: word.to_string(),
            line: line,
        }
    }
}

impl Index {

    /// Creates a new instance of Index.
    /// The contents of the instance returned is empty.
    ///
    /// # Example
    /// ```
    /// let myindex = Index::new();
    /// ```
    pub fn new() -> Index {
        Index{
            index_store: HashMap::new(),
            files: HashSet::new(),
        }
    }

    /// Adds the specified file to an already existing index.
    ///
    /// # Example
    /// ```
    /// let mut myindex = Index::new();
    /// myindex.add_file("/home/user/Documents/myfile.txt");
    /// ```
    pub fn add_file(&mut self, filename: &str) {
        if self.files.contains(filename) {
            return
        }
        let mut f = match File::open(filename) {
            Ok(fi) => fi,
            Err(e) => panic!(e),
        };
        let mut cline = 0;
        let mut contents = String::new();
        let _ = f.read_to_string(&mut contents);
        let lines: Vec<&str> = contents.lines().collect();
        for line in &lines {
            cline += 1;
            let s = line.to_string();
            let words: Vec<&str> = s.split(' ').collect();
            for w in &words {
                let ie = IndexEntry::new(filename, &w, cline as i64);

                let mut inmap = false;
                if let Some(x) = self.index_store.get_mut(&w.to_string()) {
                    x.insert(ie.clone());
                    inmap = true;
                }
                if !inmap {
                    let mut temp = HashSet::new();
                    temp.insert(ie);
                    self.index_store.insert(w.to_string(), temp);
                }
            }
        }
        self.files.insert(filename.to_string());
    }

    pub fn remove_file(&mut self, filename: &str) {
        self.files.remove(filename);
    }

    pub fn update_index(&mut self) {
        let files = self.files.clone();
        self.index_store.clear();
        self.files.clear();

        for filename in files {
            self.add_file(&filename);
        }
    }

    pub fn search_word(&self, word: &str) -> HashSet<IndexEntry> {
        if let Some(x) = self.index_store.get(&word.to_string()) {
            return x.clone();
        }
        else {
            return HashSet::new();
        }
    }

    pub fn get_files(&self) -> &HashSet<String> {
        &self.files
    }
}

static APPNAME: &'static str = "rs-indexer";
static APP_VERSION: &'static str = "0.1.0";

fn main() {
    println!("{:0} v{:1}\n", APPNAME, APP_VERSION);
    let mut index = Index::new();
    loop {
        println!("Choose from the following options:");
        println!("1.\tAdd file");
        println!("2.\tList files");
        println!("3.\tSearch word");
        println!("4.\tUpdate index");
        println!("5.\tRemove file");
        println!("0.\tQuit");
        print!("choice: ");
        let _ = io::stdout().flush();

        let mut input = String::new();
        io::stdin().read_line(&mut input).ok().expect("Could not read line");
        let choice: u32 = match input.trim().parse() {
            Ok(n) => n,
            Err(_) => {
                println!("Please enter a numeric choice");
                continue;
            },
        };
        input = String::new();
        match choice {
            1 => {
                print!("file: ");
                let _ = io::stdout().flush();
                io::stdin().read_line(&mut input).ok().expect("Could not read line");
                index.add_file(&(input.trim()));
                println!("Added file {}\n", input);
            },

            2 => {
                let ref indexed_files = *index.get_files();
                println!("Indexed files:");
                for f in indexed_files {
                    println!("file: {}", f);
                }
                println!("\n");
            },

            3 => {
                print!("word: ");
                let _ = io::stdout().flush();
                io::stdin().read_line(&mut input).ok().expect("Could not read line");
                let result = index.search_word(&(input.trim()));
                if result.len() > 0 {
                    for ie in &result {
                        println!("{}\nfile: {}\nline: {}\n", &ie.word, &ie.filename, ie.line);
                    }
                }
            },

            4 => {
                println!("Updating index...");
                index.update_index();
                println!("Index update complete\n");
            },

            5 => {
                print!("file: ");
                let _ = io::stdout().flush();
                io::stdin().read_line(&mut input).ok().expect("Could not read line");
                index.remove_file(&(input.trim()));
                println!("Removed file {}\n", input);
            },

            0 => {
                println!("Goodbye!!");
                break;
            },

            _ => {
                println!("");
                continue;
            },
        }
    }
}
